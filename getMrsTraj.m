function uavs = getMrsTraj()

    %% load data
    
    uav1Raw = readtable('logs/uav1_col_rtk.csv');
    uav2Raw = readtable('logs/uav2_col_rtk.csv');
    uav3Raw = readtable('logs/uav3_col_rtk.csv');
    uav4Raw = readtable('logs/uav4_col_rtk.csv');
    
    % stack all uav structures into a single list - you can access them by
    % index:
    %       uav2 is accessed by:    uavs{2}
    uavsRaw = {uav1Raw, uav2Raw, uav3Raw, uav4Raw};
    
    % N = 2850;
    % N = 4000;
    N = 3400;
    % N = size(uavs{1}, 1) - 100;
    % M = 750;
    M = 0;
    for i=1:4
        uavsRaw{i} = uavsRaw{i}(end - N:end - M,:);
        % uavs{i} = uavs{i}(1:end - M,:);
    end
    
    % get the t0
    t0 = inf;
    for i=1:4
        if uavsRaw{i}.x_time(1) < t0
            t0 = uavsRaw{i}.x_time(1);
        end
    end
   
    uav1 = struct;
    uav2 = struct;
    uav3 = struct;
    uav4 = struct;

    uavs = {uav1, uav2, uav3, uav4};

    for i=1:4
        % transform the time
        % uavsRaw{i}.x_time = (uavsRaw{i}.x_time - t0)/1000000000;
        uavs{i}.time = (uavsRaw{i}.x_time - t0)/1000000000;

        % uavs{i}.trajectory = [uavsRaw{i}.field_pose_pose_position_x, uavsRaw{i}.field_pose_pose_position_y, uavsRaw{i}.field_pose_pose_position_z];
        uavs{i}.trajectory.x = uavsRaw{i}.field_pose_pose_position_x;
        uavs{i}.trajectory.y = uavsRaw{i}.field_pose_pose_position_y;
        uavs{i}.trajectory.z = uavsRaw{i}.field_pose_pose_position_z - 1;
    end

    %% plot the figure
    
    % figure
    % hold on
    % grid on
    % 
    % for i=1:4
    %     % plot3(uavs{i}.field_pose_pose_position_x, uavs{i}.field_pose_pose_position_y, uavs{i}.field_pose_pose_position_z);
    %     plot3(uavs{i}.trajectory.x, uavs{i}.trajectory.y, uavs{i}.trajectory.z);
    % end
    % 
    % legend("1","2","3","4")
    % view(30,30)
    
    %% plot the relative distances

    % uav1_pos = [uavs{1}.trajectory.x, uavs{1}.trajectory.y, uavs{1}.trajectory.z];
    % uav2_pos = [uavs{2}.trajectory.x, uavs{2}.trajectory.y, uavs{2}.trajectory.z];
    % uav3_pos = [uavs{3}.trajectory.x, uavs{3}.trajectory.y, uavs{3}.trajectory.z];
    % uav4_pos = [uavs{4}.trajectory.x, uavs{4}.trajectory.y, uavs{4}.trajectory.z];
    % 
    % x = [uav1_pos.'; uav2_pos.'; uav3_pos.'; uav4_pos.'];
    % save('x.mat', "x");
    % 
    % rel_dist_1_2 = sqrt(sum(((uav1_pos - uav2_pos).*(uav1_pos - uav2_pos)), 2));

    % figure
    % hold on
    % plot(uavs{1}.time, rel_dist_1_2)

end
