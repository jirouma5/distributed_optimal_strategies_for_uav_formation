
function [x,u] = trajfun(x_0)
global T d10 d20 d31 mu10 mu20 mu31 w10 w20 w31 
% UAVs optimal trajectories and control inputs (all time no collision)

y1_0=x_0(4:6)-x_0(1:3)-d10;
y2_0=x_0(7:9)-x_0(1:3)-d20;
y3_0=x_0(10:12)-x_0(4:6)-d31;

t=0:0.1:T;

for i=1:length(t)    
      x0(:,i)=x_0(1:3)+[0.5*t(i) 0.5*t(i) t(i)]';
      u0(:,i)=[0.5 0.5   1]';
      
      xi1(:,i)=-(w10*sqrt(mu10)*cosh(sqrt(mu10)*(T-t(i)))+mu10*sinh(sqrt(mu10)*(T-t(i))))/(sqrt(mu10)*cosh(sqrt(mu10)*T)+w10*sinh(sqrt(mu10)*T))*y1_0;
      a1=(sqrt(mu10)*cosh(sqrt(mu10)*(T-t(i)))+w10*sinh(sqrt(mu10)*(T-t(i))))/(sqrt(mu10)*cosh(sqrt(mu10)*T)+w10*sinh(sqrt(mu10)*T));
      y1(:,i)=a1*y1_0;
      xd1(:,i)=x0(:,i)+y1(:,i)+d10;
      ud1(:,i)=u0(:,i)+xi1(:,i);
      

      xi2(:,i)=-(w20*sqrt(mu20)*cosh(sqrt(mu20)*(T-t(i)))+mu20*sinh(sqrt(mu20)*(T-t(i))))/(sqrt(mu20)*cosh(sqrt(mu20)*T)+w20*sinh(sqrt(mu20)*T))*y2_0;
      a2=(sqrt(mu20)*cosh(sqrt(mu20)*(T-t(i)))+w20*sinh(sqrt(mu20)*(T-t(i))))/(sqrt(mu20)*cosh(sqrt(mu20)*T)+w20*sinh(sqrt(mu20)*T));
      y2(:,i)=a2*y2_0;
      xd2(:,i)=x0(:,i)+y2(:,i)+d20;
      ud2(:,i)=u0(:,i)+xi2(:,i);

      xi3(:,i)=-(w31*sqrt(mu31)*cosh(sqrt(mu31)*(T-t(i)))+mu31*sinh(sqrt(mu31)*(T-t(i))))/(sqrt(mu31)*cosh(sqrt(mu31)*T)+w31*sinh(sqrt(mu31)*T))*y3_0;
      a3=(sqrt(mu31)*cosh(sqrt(mu31)*(T-t(i)))+w31*sinh(sqrt(mu31)*(T-t(i))))/(sqrt(mu31)*cosh(sqrt(mu31)*T)+w31*sinh(sqrt(mu31)*T));
      y3(:,i)=a3*y3_0;
      xd3(:,i)=xd1(:,i)+y3(:,i)+d31;
      ud3(:,i)=ud1(:,i)+xi3(:,i);


end



x=[x0;xd1;xd2;xd3];
u=[u0;ud1;ud2;ud3];