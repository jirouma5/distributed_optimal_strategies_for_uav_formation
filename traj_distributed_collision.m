clc
close all
clear 

global T d10 d20 d31 mu10 mu20 mu31 w10 w20 w31 
T=10; 
t=0:0.1:T;

d10=2*[-1;0;0];
d20=2*[0;-1;0];
d31=2*[0;-1;0];

r0=0.5; r1=0.5; r2=0.5; r3=0.5;
lam1=0.01;
lam2=0.01;
lam3=0.01;

d=[d10;d20;d31];


mu10=0.7; mu20=0.5; mu31=0.8; 

w10=1; w20=1; w31=1; 


%% initial positions in N-player game
x0_0=[0.5*t(1) 0.5*t(1) t(1)]';
u0_0=[0.5 0.5 1]';
x0_1=[3; 2; 0]; 
x0_2=[-2; -1; 0];
x0_3=[1; 0; 0];


%% initial positions in relative dynamics system
y1_0=x0_1-x0_0-d10;
y2_0=x0_2-x0_0-d20;
y3_0=x0_3-x0_1-d31;

x_0=[x0_0;x0_1;x0_2;x0_3];
[x,u] = trajfun(x_0);
[rdd10,rdd20,rdd31]=UAVdist(x);

figure(1)
plot(t,rdd10,'r--'); hold on
plot(t,rdd20,'b--');
plot(t,rdd31,'g--');

legend('1-0', '2-0', '3-1')

figure(2)
plot3(x(1,:),x(2,:),x(3,:)); 
hold on
plot3(x(4,:),x(5,:),x(6,:),'r--');
plot3(x(7,:),x(8,:),x(9,:),'b--');
plot3(x(10,:),x(11,:),x(12,:),'g--');

legend('0', '1', '2', '3')

%% Leader's trajectory (UAV 0)
for i=1:length(t)   
    x0(:,i)=[0.5*t(i) 0.5*t(i) t(i)]';
    u0(:,i)=[0.5 0.5   1]';
end


%% UAVs optimal trajectories and control inputs 
for i=1:length(t)   

    [x,u] = trajfun(x_0);

    [rdd10,rdd20,rdd31]=UAVdist(x);

    if (min(rdd10)<r1+r0) || (min(rdd20)<r2+r0) || (min(rdd31)<r3+r1)
        i
       xd1(:,i)=x0(:,i)+exp(lam1*t(i))*(x_0(4:6)-x_0(1:3));
       xd2(:,i)=x0(:,i)+exp(lam2*t(i))*(x_0(7:9)-x_0(1:3));
       xd3(:,i)=xd1(:,i)+exp(lam3*t(i))*(x_0(10:12)-x_0(4:6));
       ud1(:,i)=u0(:,i);
       ud2(:,i)=u0(:,i);
       ud3(:,i)=u0(:,i);
       % x_0=[x0(:,i);xd1(:,i);xd2(:,i);xd3(:,i)];
    else
        xd1(:,i)=x(4:6,2);
        xd2(:,i)=x(7:9,2);
        xd3(:,i)=x(10:12,2);
        ud1(:,i)=u(4:6,2);
        ud2(:,i)=u(7:9,2);
        ud3(:,i)=u(10:12,2);
    end
x_0=[x0(:,i);xd1(:,i);xd2(:,i);xd3(:,i)];
end

p=[x0;xd1;xd2;xd3];
[rdd10,rdd20,rdd31]=UAVdist(p);

figure(1)
plot(t,rdd10,'r'); hold on
plot(t,rdd20,'b');
plot(t,rdd31,'g');
% legend('1-0', '2-0', '3-1')





figure(2)
% plot3(x(1,:),x(2,:),x(3,:)); 
% hold on
plot3(xd1(1,:),xd1(2,:),xd1(3,:),'r');
plot3(xd2(1,:),xd2(2,:),xd2(3,:),'b');
plot3(xd3(1,:),xd3(2,:),xd3(3,:),'g');

% legend('0', '1', '2', '3')
 save('x0.mat','x0');
 save('xd1.mat','xd1');
 save('xd2.mat','xd2');
 save('xd3.mat','xd3');
 
 save('u0.mat','u0');
 save('ud1.mat','ud1');
 save('ud2.mat','ud2');
 save('ud3.mat','ud3');
 

 
