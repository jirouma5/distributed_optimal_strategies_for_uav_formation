function [rdd10,rdd20,rdd31]=UAVdist(x)

for i=1:size(x,2)
    rdd10(i)=norm(x(4:6,i)-x(1:3,i));
    rdd20(i)=norm(x(7:9,i)-x(1:3,i));
    rdd31(i)=norm(x(10:12,i)-x(4:6,i));
end
