clc
close all
clear

% %Video
% v = VideoWriter("formation.avi");
% open(v)

% T=10;
T=20;
t=0:0.1:T;
% x0=[0.5*t; 0.5*t; t];
% u0=[0.5 0.5   1]';
x0=[0.5*t; 0.5*t; t]/2;
u0=[0.5 0.5   1]'/2;
% for i=1:length(t)
%     x0(:,i)=[0.5*t(i) 0.5*t(i) t(i)]';
%     u0(:,i)=[0.5 0.5   1]';
% end

d10=2*[-1;0;0];
d20=2*[0;-1;0];
d31=2*[0;-1;0];
% d42=2*[-2;0;0];

% MRS solution
% load('x.mat','x');
% load('tMRS.mat','tMRS');

simulationTraj = getMrsTraj();

% distributed solution
load('xd1.mat','xd1');
load('xd2.mat','xd2');
load('xd3.mat','xd3');
% load('xd4.mat','xd4');
load('ud1.mat','ud1');
load('ud2.mat','ud2');
load('ud3.mat','ud3'); 
% load('ud4.mat','ud4'); 

desiredTraj = {x0, xd1, xd2, xd3};

% figure('units','normalized','outerposition',[0 0 1 1])

% MRS
% px0=x(1,1);  py0=x(2,1); pz0=x(3,1); 
% px1=x(4,1);  py1=x(5,1); pz1=x(6,1);
% px2=x(7,1);  py2=x(8,1); pz2=x(9,1);
% px3=x(10,1);  py3=x(11,1); pz3=x(12,1);
% px4=x(13,1);  py4=x(14,1); pz4=x(15,1);

% distributed
pdx0=x0(1,1);   pdy0=x0(2,1); pdz0=x0(3,1); 
pdx1=xd1(1,1);  pdy1=xd1(2,1); pdz1=xd1(3,1); 
pdx2=xd2(1,1);  pdy2=xd2(2,1); pdz2=xd2(3,1);
pdx3=xd3(1,1);  pdy3=xd3(2,1); pdz3=xd3(3,1);
% pdx4=xd4(1,1);  pdy4=xd4(2,1); pdz4=xd4(3,1);

% subplot(2,3,1)

%% plot the trajectories
figure
hold on
grey = [0.6 0.6 0.6];
colors = {'k', 'b', 'g', 'm'};

for i = 1:4
    % desired trajectory
    plot3(desiredTraj{i}(1,:), desiredTraj{i}(2,:), desiredTraj{i}(3,:), 'LineWidth', 2, 'Color', colors{i}, 'LineStyle','--');
    % simulation
    plot3(simulationTraj{i}.trajectory.x, simulationTraj{i}.trajectory.y, simulationTraj{i}.trajectory.z, 'LineWidth',2,'Color', grey,'LineStyle','--');
end

%% markers
lineHandlers = cell(4,1);
markerTypes = {'pentagram', 'square', 'diamond', '^'};
for i = 1:4
    % analytical solution
    lineHandlers{i} = plot3(desiredTraj{i}(1,1), desiredTraj{i}(2,1), desiredTraj{i}(3,1), markerTypes{i},'MarkerSize',22,'MarkerEdgeColor',colors{i},'MarkerFaceColor','none');
    plot3(desiredTraj{i}(1,end), desiredTraj{i}(2,end), desiredTraj{i}(3,end), markerTypes{i},'MarkerSize',22,'MarkerEdgeColor', colors{i},'MarkerFaceColor','none');

    % simulation
    plot3(simulationTraj{i}.trajectory.x(1), simulationTraj{i}.trajectory.y(1), simulationTraj{i}.trajectory.z(1), markerTypes{i},'MarkerSize',22,'MarkerEdgeColor', grey,'MarkerFaceColor','none');
    plot3(simulationTraj{i}.trajectory.x(end), simulationTraj{i}.trajectory.y(end), simulationTraj{i}.trajectory.z(end), markerTypes{i},'MarkerSize',22,'MarkerEdgeColor', grey,'MarkerFaceColor','none');
end

%% add text
title('UAVs','interpreter','latex')
legend([lineHandlers{1} lineHandlers{2} lineHandlers{3} lineHandlers{4} ],{'0','1','2','3'},'fontsize',18,'Location','northwest','Orientation','vertical','EdgeColor','none','Box','off');
xlabel('$x$','fontsize',24,'interpreter','latex')
ylabel('$y$','fontsize',24,'interpreter','latex')
zlabel('$z$','fontsize',24,'interpreter','latex')
grid on
set(gca,'FontSize',18)
view(10,20)

%%
% for j=1:length(t)
%      if (j==1) || (j==length(t)) 
%        set(0,'DefaultLegendAutoUpdate','off');
%        legend('off')   
%        % % MRS
%        % h0 = plot3(px0,py0,pz0,'pentagram','MarkerSize',22,'MarkerEdgeColor', [.6 .6 .6],'MarkerFaceColor','none');hold all
%        % h1 = plot3(px1,py1,pz1,'square','MarkerSize',22,'MarkerEdgeColor', [.6 .6 .6],'MarkerFaceColor','none');
%        % h2 = plot3(px2,py2,pz2,'diamond','MarkerSize',22,'MarkerEdgeColor', [.6 .6 .6],'MarkerFaceColor','none');
%        % h3 = plot3(px3,py3,pz3,'^','MarkerSize',22,'MarkerEdgeColor', [.6 .6 .6],'MarkerFaceColor','none');
%        % % % h4 = plot3(px4,py4,pz4,'o','MarkerSize',22,'MarkerEdgeColor', [.6 .6 .6],'MarkerFaceColor','none');
%       % distributed
%        hd0 = plot3(pdx0,pdy0,pdz0,'pentagram','MarkerSize',22,'MarkerEdgeColor','k','MarkerFaceColor','none');hold all
%        hd1 = plot3(pdx1,pdy1,pdz1,'square','MarkerSize',22,'MarkerEdgeColor','b','MarkerFaceColor','none');
%        hd2 = plot3(pdx2,pdy2,pdz2,'diamond','MarkerSize',22,'MarkerEdgeColor','g','MarkerFaceColor','none');
%        hd3 = plot3(pdx3,pdy3,pdz3,'^','MarkerSize',22,'MarkerEdgeColor','m','MarkerFaceColor','none');
%        % hd4 = plot3(pdx4,pdy4,pdz4,'o','MarkerSize',22,'MarkerEdgeColor','r','MarkerFaceColor','none');
% 
%        lgd=legend([hd0 hd1 hd2 hd3 ],{'0','1','2','3'},'fontsize',18,'Location','northwest','Orientation','vertical','EdgeColor','none','Box','off');
%        title(lgd,'UAVs','interpreter','latex')
% %        title('delay-free ($\tau=0$) trajectories','fontsize',16,'interpreter','latex')
% %        title('positions','fontsize',24,'interpreter','latex')
%        set(gca,'FontSize',18)
% 
%        xlabel('$x$','fontsize',24,'interpreter','latex')
%        ylabel('$y$','fontsize',24,'interpreter','latex')
%        zlabel('$z$','fontsize',24,'interpreter','latex')
% 
%      end
%     % % MRS
%     % px0=x(1,j);  py0=x(2,j); pz0=x(3,j); 
%     % px1=x(4,j);  py1=x(5,j); pz1=x(6,j);
%     % px2=x(7,j);  py2=x(8,j); pz2=x(9,j);
%     % px3=x(10,j);  py3=x(11,j); pz3=x(12,j); 
%     % % % px4=x(13,j);  py4=x(14,j); pz4=x(15,j);
%     % distributed
%     pdx0=x0(1,j);  pdy0=x0(2,j); pdz0=x0(3,j); 
%     pdx1=xd1(1,j);  pdy1=xd1(2,j); pdz1=xd1(3,j);
%     pdx2=xd2(1,j);  pdy2=xd2(2,j); pdz2=xd2(3,j); 
%     pdx3=xd3(1,j);  pdy3=xd3(2,j); pdz3=xd3(3,j); 
%     % pdx4=xd4(1,j);  pdy4=xd4(2,j); pdz4=xd4(3,j);
%    if j>1
%      % % MRS  
%      % line([x(1,j) x(1,j-1)],[x(2,j) x(2,j-1)],[x(3,j) x(3,j-1)],'LineWidth',2,'color', [.6 .6 .6],'LineStyle','-') % leader vehicle
%      % line([x(4,j) x(4,j-1)],[x(5,j) x(5,j-1)],[x(6,j) x(6,j-1)],'LineWidth',2,'color', [.6 .6 .6],'LineStyle','-')
%      % line([x(7,j) x(7,j-1)],[x(8,j) x(8,j-1)],[x(9,j) x(9,j-1)],'LineWidth',2,'color', [.6 .6 .6],'LineStyle','-')
%      % line([x(10,j) x(10,j-1)],[x(11,j) x(11,j-1)],[x(12,j) x(12,j-1)],'LineWidth',2,'color', [.6 .6 .6],'LineStyle','-')
%      % % % line([x(13,j) x(13,j-1)],[x(14,j) x(14,j-1)],[x(15,j) x(15,j-1)],'LineWidth',2,'color', [.6 .6 .6],'LineStyle','-')
%      % distributed
%      line([x0(1,j) x0(1,j-1)],[x0(2,j) x0(2,j-1)],[x0(3,j) x0(3,j-1)],'LineWidth',2,'Color','k','LineStyle','--') % leader vehicle
%      line([xd1(1,j) xd1(1,j-1)],[xd1(2,j) xd1(2,j-1)],[xd1(3,j) xd1(3,j-1)],'LineWidth',2,'Color','b','LineStyle','--') % 
%      line([xd2(1,j) xd2(1,j-1)],[xd2(2,j) xd2(2,j-1)],[xd2(3,j) xd2(3,j-1)],'LineWidth',2,'Color','g','LineStyle','--')
%      line([xd3(1,j) xd3(1,j-1)],[xd3(2,j) xd3(2,j-1)],[xd3(3,j) xd3(3,j-1)],'LineWidth',2,'Color','m','LineStyle','--')   
%      % line([xd4(1,j) xd4(1,j-1)],[xd4(2,j) xd4(2,j-1)],[xd4(3,j) xd4(3,j-1)],'LineWidth',2,'Color','r','LineStyle','--')
%    end
%     grid on
%     % pause(0.05)
%     axis([-8 16 -4 12 -2 10])
%     % M = getframe(gcf);
%     % writeVideo(v,M)
% end
% % close(v)
% 
% for j=1:length(tMRS)
%      if (j==1) || (j==length(tMRS)) 
%        set(0,'DefaultLegendAutoUpdate','off');
%        legend('off')   
%        % MRS
%        h0 = plot3(px0,py0,pz0,'pentagram','MarkerSize',22,'MarkerEdgeColor', [.6 .6 .6],'MarkerFaceColor','none');hold all
%        h1 = plot3(px1,py1,pz1,'square','MarkerSize',22,'MarkerEdgeColor', [.6 .6 .6],'MarkerFaceColor','none');
%        h2 = plot3(px2,py2,pz2,'diamond','MarkerSize',22,'MarkerEdgeColor', [.6 .6 .6],'MarkerFaceColor','none');
%        h3 = plot3(px3,py3,pz3,'^','MarkerSize',22,'MarkerEdgeColor', [.6 .6 .6],'MarkerFaceColor','none');
% 
%      end
%     % MRS
%     px0=x(1,j);  py0=x(2,j); pz0=x(3,j); 
%     px1=x(4,j);  py1=x(5,j); pz1=x(6,j);
%     px2=x(7,j);  py2=x(8,j); pz2=x(9,j);
%     px3=x(10,j);  py3=x(11,j); pz3=x(12,j); 
% 
%    if j>1
%      % MRS  
%      line([x(1,j) x(1,j-1)],[x(2,j) x(2,j-1)],[x(3,j) x(3,j-1)],'LineWidth',2,'color', [.6 .6 .6],'LineStyle','-') % leader vehicle
%      line([x(4,j) x(4,j-1)],[x(5,j) x(5,j-1)],[x(6,j) x(6,j-1)],'LineWidth',2,'color', [.6 .6 .6],'LineStyle','-')
%      line([x(7,j) x(7,j-1)],[x(8,j) x(8,j-1)],[x(9,j) x(9,j-1)],'LineWidth',2,'color', [.6 .6 .6],'LineStyle','-')
%      line([x(10,j) x(10,j-1)],[x(11,j) x(11,j-1)],[x(12,j) x(12,j-1)],'LineWidth',2,'color', [.6 .6 .6],'LineStyle','-')
%    end
%     grid on
%     % pause(0.05)
%     axis([-8 16 -4 12 -2 10])
%     % M = getframe(gcf);
%     % writeVideo(v,M)
% end
% % close(v)

figure(2)
    %  % MRS  
    % plot(x(1,:),'color', [.6 .6 .6],'LineWidth',2); hold on; 
    % plot(x(4,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    % plot(x(7,:),'color', [.6 .6 .6],'LineWidth',2); hold on; 
    % plot(x(10,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    % % plot(t,x(13,:),'color', [.6 .6 .6],'LineWidth',2'); hold on;
    %  % distributed
    plot(t,x0(1,:),'--k','LineWidth',2); hold on; 
    plot(t,xd1(1,:),'--b','LineWidth',2'); hold on; 
    plot(t,xd2(1,:),'--g','LineWidth',2); hold on; 
    plot(t,xd3(1,:),'--m','LineWidth',2'); hold on; 
    % plot(t,xd4(1,:),'--r','LineWidth',2'); hold on;
    set(gca,'FontSize',18)
    xlabel('$t$', 'FontSize',24,'Interpreter','latex');
    ylabel('positions $x(t)$', 'FontSize',24,'Interpreter','latex');

figure(3)
    %  % MRS  
    % plot(t,x(2,:),'color', [.6 .6 .6],'LineWidth',2); hold on; 
    % plot(t,x(5,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    % plot(t,x(8,:),'color', [.6 .6 .6],'LineWidth',2); hold on; 
    % plot(t,x(11,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    % % % plot(t,x(14,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    %  % distributed
    plot(t,x0(2,:),'--k','LineWidth',2); hold on;
    plot(t,xd1(2,:),'--b','LineWidth',2); hold on;
    plot(t,xd2(2,:),'--g','LineWidth',2); hold on;
    plot(t,xd3(2,:),'--m','LineWidth',2); hold on;
    % plot(t,xd4(2,:),'--r','LineWidth',2); hold on;
    set(gca,'FontSize',18)
    xlabel('$t$', 'FontSize',24,'Interpreter','latex');
    ylabel('positions $y(t)$', 'FontSize',24,'Interpreter','latex');

figure(4)
    %  % MRS  
    % plot(t,x(3,:),'color', [.6 .6 .6],'LineWidth',2); hold on; 
    % plot(t,x(6,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    % plot(t,x(9,:),'color', [.6 .6 .6],'LineWidth',2); hold on; 
    % plot(t,x(12,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    % % % plot(t,x(15,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    %  % distributed
    plot(t,x0(3,:),'--k','LineWidth',2); hold on;
    plot(t,xd1(3,:),'--b','LineWidth',2); hold on;
    plot(t,xd2(3,:),'--g','LineWidth',2); hold on;
    plot(t,xd3(3,:),'--m','LineWidth',2); hold on;
    % plot(t,xd4(3,:),'--r','LineWidth',2); hold on;
    set(gca,'FontSize',18)
    xlabel('$t$', 'FontSize',24,'Interpreter','latex');
    ylabel('positions $z(t)$', 'FontSize',24,'Interpreter','latex');


figure(5)
    %  % MRS  
    % plot(t,u0(1,:),'color', [.6 .6 .6],'LineWidth',2); hold on; 
    % plot(t,u1(1,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    % plot(t,u2(1,:),'color', [.6 .6 .6],'LineWidth',2); hold on; 
    % plot(t,u3(1,:),'color', [.6 .6 .6],'LineWidth',2'); hold on;
    % % % plot(t,u4(1,:),'color', [.6 .6 .6],'LineWidth',2'); hold on;
    %  % distributed
    plot(t,u0(1,:),'--k','LineWidth',2); hold on;
    plot(t,ud1(1,:),'--b','LineWidth',2); hold on;
    plot(t,ud2(1,:),'--g','LineWidth',2); hold on;
    plot(t,ud3(1,:),'--m','LineWidth',2); hold on;
    % plot(t,ud4(1,:),'--r','LineWidth',2); hold on;
    set(gca,'FontSize',18)
    xlabel('$t$', 'FontSize',24,'Interpreter','latex');
    ylabel('control inputs $u_{ix}(t)$', 'FontSize',24,'Interpreter','latex');


figure(6)
    %  % MRS  
    % plot(t,u0(2,:),'color', [.6 .6 .6],'LineWidth',2); hold on; 
    % plot(t,u1(2,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    % plot(t,u2(2,:),'color', [.6 .6 .6],'LineWidth',2); hold on; 
    % plot(t,u3(2,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    % % plot(t,u4(2,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    %  % distributed
    plot(t,u0(2,:),'--k','LineWidth',2); hold on;
    plot(t,ud1(2,:),'--b','LineWidth',2); hold on;
    plot(t,ud2(2,:),'--g','LineWidth',2); hold on;
    plot(t,ud3(2,:),'--m','LineWidth',2); hold on;
    % plot(t,ud4(2,:),'--r','LineWidth',2); hold on;
    set(gca,'FontSize',18)
    xlabel('$t$', 'FontSize',24,'Interpreter','latex');
    ylabel('control inputs $u_{iy}(t)$', 'FontSize',24,'Interpreter','latex');

figure(7)
    %  % MRS  
    % plot(t,u0(3,:),'color', [.6 .6 .6],'LineWidth',2); hold on; 
    % plot(t,u1(3,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    % plot(t,u2(3,:),'color', [.6 .6 .6],'LineWidth',2); hold on; 
    % plot(t,u3(3,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    % % plot(t,u4(3,:),'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    %  % distributed
    plot(t,u0(3,:),'--k','LineWidth',2); hold on;
    plot(t,ud1(3,:),'--b','LineWidth',2); hold on;
    plot(t,ud2(3,:),'--g','LineWidth',2); hold on;
    plot(t,ud3(3,:),'--m','LineWidth',2); hold on;
    % plot(t,ud4(3,:),'--r','LineWidth',2); hold on;
    set(gca,'FontSize',18)
    xlabel('$t$', 'FontSize',24,'Interpreter','latex');
    ylabel('control inputs $u_{iz}(t)$', 'FontSize',24,'Interpreter','latex');
   
    
% [rd10,rd20,rd31]=UAVdist(x);

p=[x0; xd1; xd2; xd3];
[rdd10,rdd20,rdd31]=UAVdist(p);

a=ones(length(t),1);
figure(8)
    %  % non-distributed  
    plot(t,a,'color', [.6 .6 .6],'LineWidth',2); hold on; 
    % plot(t,rd20,'color', [.6 .6 .6],'LineWidth',2'); hold on; 
    % plot(t,rd31,'color', [.6 .6 .6],'LineWidth',2); hold on;  
     % distributed
    plot(t,rdd10,'--r','LineWidth',2); hold on; 
    plot(t,rdd20,'--b','LineWidth',2'); hold on; 
    plot(t,rdd31,'--g','LineWidth',2); hold on; 

    set(gca,'FontSize',18)
    title('single-integrator UAV model')
    xlabel('$t$', 'FontSize',24,'Interpreter','latex');
    ylabel('relative distances', 'FontSize',24,'Interpreter','latex');
    legend({'','$\|x_1(t)-x_0(t)\|$','$\|x_2(t)-x_0(t)\|$','$\|x_3(t)-x_1(t)\|$'},'fontsize',16,'Location','southeast','Orientation','vertical','EdgeColor','none','Box','off','Interpreter','latex');


% [rdd10,rdd20,rdd31]=UAVdist(x);

uavTrajs = cell(4,1);
for i = 1:4
    uavTrajs{i} = [simulationTraj{i}.trajectory.x simulationTraj{i}.trajectory.y simulationTraj{i}.trajectory.z];
end
rdd10 = sqrt(sum(((uavTrajs{1} - uavTrajs{2}).*(uavTrajs{1} - uavTrajs{2})), 2));
rdd20 = sqrt(sum(((uavTrajs{1} - uavTrajs{3}).*(uavTrajs{1} - uavTrajs{3})), 2));
rdd31 = sqrt(sum(((uavTrajs{2} - uavTrajs{4}).*(uavTrajs{2} - uavTrajs{4})), 2));

a=ones(length(simulationTraj{1}.time),1);
% figure(9)
plot(simulationTraj{1}.time ,a,'color', [.6 .6 .6],'LineWidth',2);
% hold on; 
plot(simulationTraj{1}.time,rdd10,'--r','LineWidth',2); hold on; 
plot(simulationTraj{1}.time,rdd20,'--b','LineWidth',2'); hold on; 
plot(simulationTraj{1}.time,rdd31,'--g','LineWidth',2); hold on; 

set(gca,'FontSize',18)
title('MRS UAV System')
xlabel('$t$', 'FontSize',24,'Interpreter','latex');
ylabel('relative distances', 'FontSize',24,'Interpreter','latex');
legend({'','$\|x_1(t)-x_0(t)\|$','$\|x_2(t)-x_0(t)\|$','$\|x_3(t)-x_1(t)\|$'},'fontsize',16,'Location','southeast','Orientation','vertical','EdgeColor','none','Box','off','Interpreter','latex');
axis([0 20 0 15])

%% export to .csv

writematrix([simulationTraj{1}.time, a, rdd10, rdd20, rdd31l], 'test.csv');